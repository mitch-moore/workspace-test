import React, { useState, useRef, useEffect } from 'react';
import { useInterval } from './useInterval';
import { CANVAS_SIZE, SNAKE_START, APPLE_START, SCALE, SPEED, DIRECTIONS } from './constants';
import './App.css';

const App = () => {

  const canvasRef = useRef(null);

  const [snake, setSnake] = useState(SNAKE_START);
  const [apple, setApple] = useState(APPLE_START);
  const [dir, setDir] = useState([0, -1]);
  const [speed, setSpeed] = useState(null);
  const [gameOver, setGameOver] = useState(false);
  const [score, setScore] = useState(0);

  const startGame = () => {
    setSnake(SNAKE_START);
    setApple(APPLE_START);
    setDir([0, -1]);
    setSpeed(SPEED);
    setScore(0);
    setGameOver(false);
  };

  const endGame = () => {
    setSpeed(null);
    setGameOver(true);
  };

  const moveSnake = ({ keyCode }) => {
    if (keyCode >= 37 && keyCode <= 40) {
      setDir(DIRECTIONS[keyCode]);
    }
  };

  const createApple = () => {
    // useEffect handles the display of the apple
    // We need to just change the apple location
    // This is how they did it in the tutorial. Interesting approach
    // return apple.map((_a, i) => Math.floor(Math.random() * (CANVAS_SIZE[i] / SCALE)));
    return [Math.floor(Math.random() * ((CANVAS_SIZE[0] / SCALE) + 1)), Math.floor(Math.random() * ((CANVAS_SIZE[0] / SCALE) + 1))];
  };

  // Check if we collide with wall or the snake itself
  const checkCollision = (piece, snk = snake) => {
    // Check if the head of the snake collides with a wall
    if (
      piece[0] * SCALE >= CANVAS_SIZE[0] ||
      piece[0] < 0 ||
      piece[1] * SCALE >= CANVAS_SIZE[1] ||
      piece[1] < 0
    ) {
      return true;
    }

    // Check if the snake runs into itself
    // Using for loop becasue you can't break out of a forEach
    for (const segment of snk) {
      if (piece[0] === segment[0] && piece[1] === segment[1]) {
        return true;
      }
    }

    // Made it this far so we are in the clear!
    return false;
  };

  const checkAppleCollision = (newSnake, snk = snake) => {
    if (newSnake[0][0] === apple[0] && newSnake[0][1] === apple[1]) {
      let newApple = createApple();
      while (checkCollision(newApple, newSnake)) {
        newApple = createApple();
      }
      setApple(newApple);
      return true;
    }

    return false;
  };

  const gameLoop = () => {
    // This will create the idea of motion for the snake.
    // We add a new head while removoing the last snake item to make it look like
    // it is moving forward
    const snakeCopy = JSON.parse(JSON.stringify(snake));
    const newSnakeHead = [snakeCopy[0][0] + dir[0], snakeCopy[0][1] + dir[1]];
    snakeCopy.unshift(newSnakeHead);

    if (checkCollision(newSnakeHead)) {
      endGame();
    }

    // checkAppleCollision handles new apple creation
    if (checkAppleCollision(snakeCopy)) {
      // Add to the user's score
      setScore(score + 1);
    } else {
      snakeCopy.pop();
    }
    
    setSnake(snakeCopy);
  };

  useEffect(() => {
    const context = canvasRef.current.getContext("2d");
    context.setTransform(SCALE, 0, 0, SCALE, 0, 0);
    context.clearRect(0, 0, CANVAS_SIZE[0], CANVAS_SIZE[1]);

    context.fillStyle = "pink";
    snake.forEach(([x, y]) => context.fillRect(x, y, 1, 1));

    context.fillStyle = "lightblue";
    context.fillRect(apple[0], apple[1], 1, 1);
  }, [snake, apple, gameOver]);

  useInterval(() => gameLoop(), speed);

  return (
    <div role="button" tabIndex="0" onKeyDown={e => moveSnake(e)}>
      <canvas
        style={{border: "1px solid black"}} 
        ref={canvasRef}
        width={`${CANVAS_SIZE[0]}px`}
        height={`${CANVAS_SIZE[1]}px`}
      />
      {gameOver && <div>GAME OVER!!</div>}
      <button onClick={startGame}>
        Start Game
      </button>
    </div>
  );
}

export default App;
